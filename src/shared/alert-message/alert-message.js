export default {
  props: {
    type: {
      type: Boolean,
      required: true
    },
    message: {
      type: String,
      required: true
    }
  }
};
