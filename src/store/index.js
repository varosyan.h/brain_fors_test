import Vue from "vue";
import Vuex from "vuex";
import Seat from "./modules/seat";
Vue.use(Vuex);

export default new Vuex.Store({
  modules: [Seat]
});
