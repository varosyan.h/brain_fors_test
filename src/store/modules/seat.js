export default {
  state: {
    seatList: [
      {
        rowId: 1,
        rowNum: 1,
        rowSeats: [
          {
            price: 2000,
            isFree: false,
            seatId: 1
          },
          {
            price: 1500,
            isFree: true,
            seatId: 2
          },
          {
            price: 1500,
            isFree: false,
            seatId: 3
          },
          {
            price: 1000,
            isFree: true,
            seatId: 4
          },
          {
            price: 1500,
            isFree: false,
            seatId: 5
          },
          {
            price: 1500,
            isFree: true,
            seatId: 6
          },
          {
            price: 1500,
            isFree: false,
            seatId: 7
          },
          {
            price: 1500,
            isFree: true,
            seatId: 8
          }
        ]
      },
      {
        rowId: 2,
        rowNum: 2,
        rowSeats: [
          {
            price: 2000,
            isFree: true,
            seatId: 1
          },
          {
            price: 1500,
            isFree: false,
            seatId: 2
          },
          {
            price: 1500,
            isFree: false,
            seatId: 3
          },
          {
            price: 1000,
            isFree: false,
            seatId: 4
          },
          {
            price: 1500,
            isFree: false,
            seatId: 5
          },
          {
            price: 1500,
            isFree: true,
            seatId: 6
          },
          {
            price: 1500,
            isFree: false,
            seatId: 7
          },
          {
            price: 1500,
            isFree: false,
            seatId: 8
          }
        ]
      },
      {
        rowId: 3,
        rowNum: 3,
        rowSeats: [
          {
            price: 2000,
            isFree: false,
            seatId: 1
          },
          {
            price: 1500,
            isFree: false,
            seatId: 2
          },
          {
            price: 1500,
            isFree: false,
            seatId: 3
          },
          {
            price: 1000,
            isFree: true,
            seatId: 4
          },
          {
            price: 1500,
            isFree: false,
            seatId: 5
          },
          {
            price: 1500,
            isFree: false,
            seatId: 6
          },
          {
            price: 1500,
            isFree: false,
            seatId: 7
          },
          {
            price: 1500,
            isFree: true,
            seatId: 8
          }
        ]
      },
      {
        rowId: 4,
        rowNum: 4,
        rowSeats: [
          {
            price: 2000,
            isFree: false,
            seatId: 1
          },
          {
            price: 1500,
            isFree: false,
            seatId: 2
          },
          {
            price: 1500,
            isFree: false,
            seatId: 3
          },
          {
            price: 1000,
            isFree: false,
            seatId: 4
          },
          {
            price: 1500,
            isFree: false,
            seatId: 5
          },
          {
            price: 1500,
            isFree: true,
            seatId: 6
          },
          {
            price: 1500,
            isFree: false,
            seatId: 7
          },
          {
            price: 1500,
            isFree: false,
            seatId: 8
          }
        ]
      },
      {
        rowId: 5,
        rowNum: 5,
        rowSeats: [
          {
            price: 2000,
            isFree: false,
            seatId: 1
          },
          {
            price: 1500,
            isFree: true,
            seatId: 2
          },
          {
            price: 1500,
            isFree: false,
            seatId: 3
          },
          {
            price: 1000,
            isFree: false,
            seatId: 4
          },
          {
            price: 1500,
            isFree: true,
            seatId: 5
          },
          {
            price: 1500,
            isFree: false,
            seatId: 6
          },
          {
            price: 1500,
            isFree: false,
            seatId: 7
          },
          {
            price: 1500,
            isFree: true,
            seatId: 8
          }
        ]
      },
      {
        rowId: 6,
        rowNum: 6,
        rowSeats: [
          {
            price: 2000,
            isFree: false,
            seatId: 1
          },
          {
            price: 1500,
            isFree: false,
            seatId: 2
          },
          {
            price: 1500,
            isFree: false,
            seatId: 3
          },
          {
            price: 1000,
            isFree: true,
            seatId: 4
          },
          {
            price: 1500,
            isFree: false,
            seatId: 5
          },
          {
            price: 1500,
            isFree: false,
            seatId: 6
          },
          {
            price: 1500,
            isFree: false,
            seatId: 7
          },
          {
            price: 1500,
            isFree: false,
            seatId: 8
          }
        ]
      }
    ]
  },
  mutations: {
    SET_SEAT_LIST(state, newList) {
      state.seatList = newList;
    },
    CHANGE_SEAT_STATE(state, seatData) {
      const { seatList } = state;
      const { rowId, seatId, isFree } = seatData;
      const rowIndex = seatList.findIndex(e => e.rowId === rowId);
      if (rowIndex > -1) {
        const row = seatList[rowIndex];
        const seatIndex = row.rowSeats.findIndex(e => e.seatId === seatId);
        if (seatIndex > -1) {
          state.seatList[rowIndex].rowSeats[seatIndex].isFree = isFree;
        } else {
          alert("Сиденя не Найден");
        }
      } else {
        alert("Ряд не Найден");
      }
    }
  },
  actions: {},
  getters: {
    GET_SEAT_LIST: state => state.seatList
  }
};
