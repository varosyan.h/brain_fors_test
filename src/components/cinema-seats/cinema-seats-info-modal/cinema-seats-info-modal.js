import { mapMutations } from "vuex";
export default {
  props: {
    seatsInfo: {
      required: true,
      type: Object
    },
    showAlert: {
      type: Function,
      required: true
    }
  },
  methods: {
    ...mapMutations(["CHANGE_SEAT_STATE"]),
    onClickCancel() {
      this.$emit("close");
    },
    onClickBuy() {
      const data = {
        rowId: this.seatsInfo.rowId,
        seatId: this.seatsInfo.seatId,
        isFree: true
      };
      this.CHANGE_SEAT_STATE(data);
      this.showAlert(true);
      this.$emit("close");
    },
    cancelTheOrder() {
      const data = {
        rowId: this.seatsInfo.rowId,
        seatId: this.seatsInfo.seatId,
        isFree: false
      };
      this.CHANGE_SEAT_STATE(data);
      this.showAlert(false);
      this.$emit("close");
    }
  }
};
