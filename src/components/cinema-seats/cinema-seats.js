import CinemaSeatsModalInfo from "./cinema-seats-info-modal/cinema-seats-info-modal.vue";
export default {
  props: {
    cinemaInfo: {
      type: Object,
      required: true
    }
  },
  methods: {
    openCinemaModal() {
      this.$modal.show(
        CinemaSeatsModalInfo,
        {
          seatsInfo: {
            indexRowCinema: this.cinemaInfo.indexRowCinema,
            indexSeats: this.cinemaInfo.indexSeats,
            price: this.cinemaInfo.price,
            isFree: this.cinemaInfo.isFree,
            seatId: this.cinemaInfo.seatId,
            rowId: this.cinemaInfo.rowId
          },
          showAlert: e => {
            this.$emit("showAlert", e);
          }
        },
        {
          width: 500,
          height: "auto"
        }
      );
    }
  }
};
